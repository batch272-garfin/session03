[Section] Inserting Records

-- inserting data in one column

INSERT INTO artists (name) VALUES ("Rivermaya");

INSERT INTO artists (name) VALUES ("PSY");

-- inserting data in one or two columns

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Psy 6", "2012-1-1", 2);

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Trip", "1996-1-1", 1);

INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Gangnam Style", 253, "K-pop", 1);

INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Ulan", 323, "OPM", 2);

INSERT INTO songs (song_name, length, genre, album_id) VALUES ("214", 403, "OPM", 2);


[Section] Displaying Records

-- SELECT column_name1, column_name2 FROM table_name;

SELECT song_name, genre FROM songs;

-- SELECT column_name FROM table_name WHERE condition;

SELECT song_name FROM songs WHERE genre = "OPM";

-- SELECT * FROM table_name;

SELECT * FROM songs;

-- SELECT column_name FROM table_name WHERE condition1 AND condition2;

SELECT song_name, length FROM songs WHERE length > 200 AND genre = "OPM";


[Section] Updating Records

-- UPDATE table_name SET column_name WHERE condition;

UPDATE songs SET length = 259 WHERE song_name = "214";

UPDATE songs SET song_name = "Kundiman" WHERE song_name = "Ulan";


[Section] Deleting Records

-- DELETE FROM table_name WHERE condition1 AND condition2;

DELETE FROM songs WHERE genre = "OPM" AND length > 300;

-- DELETE * FROM table_name;

DELETE * FROM songs;

-- DELETE * FROM table_name WHERE condition;

DELETE * FROM songs WHERE genre = "K-pop";